<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::resource('tugasakhir', \App\Http\Controllers\TugasakhirController::class);
Route::resource('mahasiswa', \App\Http\Controllers\MahasiswaController::class);
Route::resource('dosen', \App\Http\Controllers\DosenController::class);
Route::resource('topik', \App\Http\Controllers\TopikController::class);
