<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Mahasiswa;

class MahasiswaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $mahasiswas = Mahasiswa::orderBy('id', 'DESC')->paginate(5);
        return view('mahasiswa.index', compact('mahasiswas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('mahasiswa.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nimmhs' => 'required',
            'namamhs' => 'required',
            'angkatan' => 'required',
            'email' => 'required',
            'nomorhp' => 'required',
            'idtelegram' => 'required',
        ]);

        $array = $request->only([
            'nimmhs', 'namamhs', 'angkatan', 'email', 'nomorhp', 'idtelegram'
        ]);

        $mahasiswas = Mahasiswa::create($array);
        return redirect()->route('mahasiswa.index')->with('success_message', 'Berhasil menambahkan Data Mahasiswa');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $mahasiswa = Mahasiswa::find($id);
        $mahasiswas = Mahasiswa::pluck('namamhs', 'namamhs')->all();
        if (!$mahasiswas)
            return redirect()->route('mahasiswa.index')->with('error_message', 'jabatan dengan id' . $id . 'tidak ditemukan');
        return view('mahasiswa.edit', compact('mahasiswa'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nimmhs' => 'required',
            'namamhs' => 'required',
            'angkatan' => 'required',
            'email' => 'required',
            'nomorhp' => 'required',
            'idtelegram' => 'required',
        ]);

        $input = $request->all();
        $mahasiswas = Mahasiswa::find($id);
        $mahasiswas->nimmhs = $request->nimmhs;
        $mahasiswas->namamhs = $request->namamhs;
        $mahasiswas->angkatan = $request->angkatan;
        $mahasiswas->email = $request->email;
        $mahasiswas->nomorhp = $request->nomorhp;
        $mahasiswas->idtelegram = $request->idtelegram;
        $mahasiswas->update($input);
        $mahasiswas->save();
        return redirect()->route('mahasiswa.index', compact('mahasiswas'))->with('success_message', 'Berhasil mengubah mahasiswa');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $mahasiswa = Mahasiswa::find($id);
        if (!$mahasiswa)
            return redirect()->route('mahasiswa.index')
                ->with('error_message', 'Data tidak di temukan');
        else $mahasiswa->delete();

        return redirect()->route('mahasiswa.index')->with('success_message', 'Berhasil menghapus data mahasiswa');
    }
}
