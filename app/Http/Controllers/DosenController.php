<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Dosen;

class DosenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dosens = Dosen::orderBy('id','DESC')->paginate(5);
        return view('dosen.index',compact('dosens'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dosen.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nip' => 'required',
            'namadsn' => 'required',
            'status' => 'required',
            'email' => 'required',
            'nomorhp' => 'required',
            'idtelegram' => 'required',
            'alamat' => 'required',
        ]);
        $array = $request->only([
            'nip', 'namadsn', 'status', 'email', 'nomorhp', 'idtelegram', 'alamat'
        ]);
        // dd($array);
        //$array['roles'] = $array['roles'][0];
        //$array['password'] = bcrypt($array['password']);
        //dd($array);
        $dosens = Dosen::create($array);
        return redirect()->route('dosen.index')
            ->with('success_message', 'Berhasil menambahkan data dosen baru');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $dosen = Dosen::find($id);
        $dosens = Dosen::pluck('namadsn','namadsn')->all();
        //$userRole = $user->roles->pluck('name','name')->all();
        // dd($user);
        // dd($userRole);
        if (!$dosen) return redirect()->route('dosen.index')
            ->with('error_message', 'dosen dengan id'.$id.' tidak ditemukan');
        return view('dosen.edit', compact('dosen'));
        // return view('managementusers.form', compact('user','roles','userRole'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nip' => 'required',
            'namadsn' => 'required',
            'status' => 'required',
            'email' => 'required',
            'nomorhp' => 'required',
            'idtelegram' => 'required',
            'alamat' => 'required',
        ]);
        $input = $request->all();
        $dosen = Dosen::find($id);
        $dosen->nip = $request->nip;
        $dosen->namadsn = $request->namadsn;
        $dosen->status = $request->status;
        $dosen->email = $request->email;
        $dosen->nomorhp = $request->nomorhp;
        $dosen->idtelegram = $request->idtelegram;
        $dosen->alamat = $request->alamat;
        $dosen->update($input);
        //DB::table('model_has_roles')->where('model_id',$id)->delete();
        //$user->assignRole($request->input('roles'));
       // if ($request->password) $user->password = bcrypt($request->password);
        $dosen->save();
        return redirect()->route('dosen.index',compact('dosen'))
            ->with('success_message', 'Berhasil mengubah dosen');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $dosen = Dosen::find($id);
        //dd($permissionmaster);

        if (!$dosen) 
            return redirect()->route('dosen.index')
            ->with('error_message', 'Data tidak di temukan');
        else $dosen->delete();

        return redirect()->route('dosen.index')->with('success_message','Berhasil menghapus data dosen');
    }
}
