<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Topik;

class TopikController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $topiks = Topik::orderBy('id', 'DESC')->paginate(5);
        return view('topik.index', compact('topiks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('topik.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'namatopik' => 'required',
            'deskripsi' => 'required',
        ]);

        $array = $request->only([
            'namatopik', 'deskripsi'
        ]);

        $topiks = Topik::create($array);
        return redirect()->route('topik.index')->with('success_message', 'Berhasil menambahkan Data Topik Baru');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $topik = Topik::find($id);
        $topiks = Topik::pluck('namatopik', 'namatopik')->all();
        if (!$topik) return redirect()->route('topik.index')
            ->with('error_message', 'Topik dengan id' . $id . ' tidak ditemukan');
        return view('topik.edit', compact('topik'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'namatopik' => 'required',
            'deskripsi' => 'required',

        ]);
        $input = $request->all();
        $topik = Topik::find($id);
        $topik->namatopik = $request->namatopik;
        $topik->deskripsi = $request->deskripsi;
        $topik->save();
        return redirect()->route('topik.index', compact('topik'))
            ->with('success_message', 'Berhasil mengubah Topik');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $topik = Topik::find($id);

        if (!$topik)
            return redirect()->route('topik.index')
                ->with('error_message', 'Data tidak di temukan');
        else $topik->delete();

        return redirect()->route('topik.index')->with('success_message', 'Berhasil menghapus data Topik');
    }
}
