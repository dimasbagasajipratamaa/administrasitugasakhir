<?php

namespace App\Http\Controllers;

use App\Models\Tugasakhir;
use Illuminate\Http\Request;

class TugasakhirController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tugasakhirs = Tugasakhir::orderBy('id','DESC')->paginate(5);
        return view('tugasakhir.index',compact('tugasakhirs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tugasakhir.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'namamhs' => 'required',
            'nimmhs' => 'required',
            'dospemsatu' => 'required',
            'dospemdua' => 'required',
            'topikta' => 'required',
            'judulta' => 'required',
            'tanggalmulai' => 'required',
            'tanggsalselesai' => 'required',
        ]);
        $array = $request->only([
            'namamhs', 'nimmhs', 'dospemsatu', 'dospemdua', 'topikta', 'judulta', 'tanggalmulai', 'tanggsalselesai'
        ]);
        // dd($array);
        //$array['roles'] = $array['roles'][0];
        //$array['password'] = bcrypt($array['password']);
        //dd($array);
        $tugasakhirs = Tugasakhir::create($array);
        return redirect()->route('tugasakhir.index')
            ->with('success_message', 'Berhasil menambahkan data tugas akhir baru');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tugasakhir = Tugasakhir::find($id);
        $tugasakhirs = Tugasakhir::pluck('namamhs','namamhs')->all();
        //$userRole = $user->roles->pluck('name','name')->all();
        // dd($user);
        // dd($userRole);
        if (!$tugasakhir) return redirect()->route('tugasakhir.index')
            ->with('error_message', 'tugas akhir dengan id'.$id.' tidak ditemukan');
        return view('tugasakhir.edit', compact('tugasakhir'));
        // return view('managementusers.form', compact('user','roles','userRole'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'namamhs' => 'required',
            'nimmhs' => 'required',
            'dospemsatu' => 'required',
            'dospemdua' => 'required',
            'topikta' => 'required',
            'judulta' => 'required',
            'tanggalmulai' => 'required',
            'tanggsalselesai' => 'required',
        ]);
        $input = $request->all();
        $tugasakhir = Tugasakhir::find($id);
        $tugasakhir->namamhs = $request->namamhs;
        $tugasakhir->nimmhs = $request->nimmhs;
        $tugasakhir->dospemsatu = $request->dospemsatu;
        $tugasakhir->dospemdua = $request->dospemdua;
        $tugasakhir->topikta = $request->topikta;
        $tugasakhir->judulta = $request->judulta;
        $tugasakhir->tanggalmulai = $request->tanggalmulai;
        $tugasakhir->tanggsalselesai = $request->tanggsalselesai;
        $tugasakhir->update($input);
        //DB::table('model_has_roles')->where('model_id',$id)->delete();
        //$user->assignRole($request->input('roles'));
       // if ($request->password) $user->password = bcrypt($request->password);
        $tugasakhir->save();
        return redirect()->route('tugasakhir.index',compact('tugasakhir'))
            ->with('success_message', 'Berhasil mengubah tugas$tugasakhir');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tugasakhir = Tugasakhir::find($id);
        //dd($permissionmaster);

        if (!$tugasakhir) 
            return redirect()->route('tugasakhir.index')
            ->with('error_message', 'Data tidak di temukan');
        else $tugasakhir->delete();

        return redirect()->route('tugasakhir.index')->with('success_message','Berhasil menghapus data mahasiswa');
    }
}
