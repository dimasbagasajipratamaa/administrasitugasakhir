@extends('adminlte::page')

@section('title', 'Tambah Topik Tugas Akhir')

@section('content_header')
    <h1 class="m-0 text-darkj">Tambah Topik Tugas Akhir</h1>
@stop
@section('content')
    <form action="{{route('topik.store')}}" method="post">
        @csrf
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="form-group">
                        <label for="namatopik">Nama Topik</label>
                        <input type="text" class="form-control @error('namatopik') is-invalid @enderror" id="namatopik" placeholder="Nama Topik" name="namatopik" value="{{old('namatopik')}}">
                        @error('namatopik') <span class="text-danger">{{$message}}</span> @enderror
                    </div>
                    <div class="form-group">
                        <label for="deskripsi">Deskripsi</label>
                        <input type="text" class="form-control @error('deskripsi') is-invalid @enderror" id="deskripsi" placeholder="Deskripsi Topik" name="deskripsi" value="{{old('deskripsi')}}">
                        @error('deskripsi') <span class="text-danger">{{$message}}</span> @enderror
                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                    <a href="{{route('topik.index')}}" class="btn btn-default">
                        Batal
                    </a>
                </div>
            </div>
        </div>
    </div>
@stop