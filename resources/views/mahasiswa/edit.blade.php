@extends('adminlte::page')

@section('title', 'Edit Data Mahasiswa')

@section('content_header')
    <h1 class="m-0 text-darkj">Edit Data Mahasiswa</h1>
@stop
@section('content')
    <form action="{{route('mahasiswa.update',['mahasiswa'=>$mahasiswa->id])}}" method="POST">
        @method('PUT')
        @csrf
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="form-group">
                        <label for="nimmhs">NIM</label>
                        <input type="text" class="form-control @error('nimmhs') is-invalid @enderror" id="nimmhs" placeholder="NIM" name="nimmhs" value="{{$mahasiswa->nimmhs??old('nimmhs')}}">
                        @error('nimmhs') <span class="text-danger">{{$message}}</span> @enderror
                    </div>
                    <div class="form-group">
                        <label for="namamhs">Nama</label>
                        <input type="text" class="form-control @error('namamhs') is-invalid @enderror" id="namamhs" placeholder="Nama Lengkap" name="namamhs" value="{{$mahasiswa->namamhs??old('namamhs')}}">
                        @error('namamhs') <span class="text-danger">{{$message}}</span> @enderror
                    </div>
                    <div class="form-group">
                        <label for="angkatan">Angkatan</label>
                        <input type="text" class="form-control @error('angkatan') is-invalid @enderror" id="angkatan" placeholder="Input angkatan" name="angkatan" value="{{$mahasiswa->angkatan??old('angkatan')}}">
                        @error('angkatan') <span class="text-danger">{{$message}}</span> @enderror
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="text" class="form-control @error('email') is-invalid @enderror" id="email" placeholder="Input Email" name="email" value="{{$mahasiswa->email??old('email')}}">
                        @error('email') <span class="text-danger">{{$message}}</span> @enderror
                    </div>
                    <div class="form-group">
                        <label for="nomorhp">Nomor HP</label>
                        <input type="text" class="form-control @error('nomorhp') is-invalid @enderror" id="nomorhp" placeholder="Input Nomor HP" name="nomorhp" value="{{$mahasiswa->nomorhp??old('nomorhp')}}">
                        @error('nomorhp') <span class="text-danger">{{$message}}</span> @enderror
                    </div>
                    <div class="form-group">
                        <label for="idtelegram">ID Telegram</label>
                        <input type="text" class="form-control @error('idtelegram') is-invalid @enderror" id="idtelegram" placeholder="Input ID Telegram" name="idtelegram" value="{{$mahasiswa->idtelegram??old('idtelegram')}}">
                        @error('idtelegram') <span class="text-danger">{{$message}}</span> @enderror
                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                    <a href="{{route('mahasiswa.index')}}" class="btn btn-default">
                        Batal
                    </a>
                </div>
            </div>
        </div>
    </div>
@stop