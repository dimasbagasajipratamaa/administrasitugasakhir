@extends('adminlte::page')

@section('title', 'Data Mahasiswa')

@section('content_header')
<h1>Data Mahasiswa</h1>
@stop
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                <a href="{{route('mahasiswa.create')}}" class="btn btn-primary mb-2">
                        Tambah Data Mahasiswa
                    </a>
                    <table class="table table-hover table-bordered table-stripped" id="example2">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>NIM</th>
                            <th>Nama Mahasiswa</th>
                            <th>Angkatan</th>
                            <th>Email</th>
                            <th>Nomor HP</th>
                            <th>ID Telegram</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($mahasiswas as $key => $mahasiswa)
                            <tr>
                                <td>{{$key+1}}</td>
                                <td>{{$mahasiswa->nimmhs}}</td>
                                <td>{{$mahasiswa->namamhs}}</td>
                                <td>{{$mahasiswa->angkatan}}</td>
                                <td>{{$mahasiswa->email}}</td>
                                <td>{{$mahasiswa->nomorhp}}</td>
                                <td>{{$mahasiswa->idtelegram}}</td>
                                <td>
                                    <a href="{{route('mahasiswa.edit', $mahasiswa)}}" class="btn btn-primary btn-sm">
                                        Edit
                                    </a>
                                    <a href="{{route('mahasiswa.destroy', $mahasiswa)}}" onclick="notificationBeforeDelete(event, this)" class="btn btn-danger btn-sm">
                                        Delete
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop
@push('js')
    <form action="" id="delete-form" method="post">
        @method('delete')
        @csrf
    </form>
    <script>
        $('#example2').DataTable({
            "responsive": true,
        });
        function notificationBeforeDelete(event, el) {
            event.preventDefault();
            if (confirm('Apakah anda yakin akan menghapus data ? ')) {
                $("#delete-form").attr('action', $(el).attr('href'));
                $("#delete-form").submit();
            }
        }
    </script>
@endpush