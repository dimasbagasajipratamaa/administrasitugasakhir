@extends('adminlte::page')

@section('title', 'Data Dosen')

@section('content_header')
<h1>Data Dosen</h1>
@stop
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                <a href="{{route('dosen.create')}}" class="btn btn-primary mb-2">
                        Tambah Data Dosen
                    </a>
                    <table class="table table-hover table-bordered table-stripped" id="example2">
                        <thead>
                        <tr>
                            <th>No.</th>
                            <th>NIP</th>
                            <th>Nama Dosen</th>
                            <th>Status</th>
                            <th>Email</th>
                            <th>Nomor HP</th>
                            <th>ID Telegram</th>
                            <th>Alamat</th>
                            <th>Opsi</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($dosens as $key => $dosen)
                            <tr>
                                <td>{{$key+1}}</td>
                                <td>{{$dosen->nip}}</td>
                                <td>{{$dosen->namadsn}}</td>
                                <td>{{$dosen->status}}</td>
                                <td>{{$dosen->email}}</td>
                                <td>{{$dosen->nomorhp}}</td>
                                <td>{{$dosen->idtelegram}}</td>
                                <td>{{$dosen->alamat}}</td>
                                <td>
                                    <a href="{{route('dosen.edit', $dosen)}}" class="btn btn-primary btn-sm">
                                        Edit
                                    </a>
                                    <a href="{{route('dosen.destroy', $dosen)}}" onclick="notificationBeforeDelete(event, this)" class="btn btn-danger btn-sm">
                                        Delete
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop
@push('js')
    <form action="" id="delete-form" method="post">
        @method('delete')
        @csrf
    </form>
    <script>
        $('#example2').DataTable({
            "responsive": true,
        });
        function notificationBeforeDelete(event, el) {
            event.preventDefault();
            if (confirm('Apakah anda yakin akan menghapus data ? ')) {
                $("#delete-form").attr('action', $(el).attr('href'));
                $("#delete-form").submit();
            }
        }
    </script>
@endpush