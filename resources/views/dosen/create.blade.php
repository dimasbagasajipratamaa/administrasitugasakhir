@extends('adminlte::page')

@section('title', 'Tambah Data Dosen')

@section('content_header')
    <h1 class="m-0 text-darkj">Tambah Data Dosen</h1>
@stop
@section('content')
    <form action="{{route('dosen.store')}}" method="post">
        @csrf
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">

                    <div class="form-group">
                        <label for="exampleInputNip">NIP</label>
                        <input type="text" class="form-control @error('nip') is-invalid @enderror" id="exampleInputNip" placeholder="Input NIP" name="nip" value="{{old('nip')}}">
                        @error('nip') <span class="text-danger">{{$message}}</span> @enderror
                    </div>
                    <div class="form-group">
                        <label for="exampleInputNamaDosen">Nama Dosen</label>
                        <input type="text" class="form-control @error('namadsn') is-invalid @enderror" placeholder="Input Nama Dosen" name="namadsn" value="{{old('namadsn')}}">
                        @error('namadsn') <span class="text-danger">{{$message}}</span> @enderror
                    </div>
                    <div class="form-group">
                        <label for="exampleInputStatus">Status</label>
                        <input type="text" class="form-control @error('status') is-invalid @enderror" placeholder="Input Status" name="status" value="{{old('status')}}">
                        @error('status') <span class="text-danger">{{$message}}</span> @enderror
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail">Email</label>
                        <input type="text" class="form-control @error('email') is-invalid @enderror" placeholder="Input Email" name="email" value="{{old('email')}}">
                        @error('email') <span class="text-danger">{{$message}}</span> @enderror
                    </div>
                    <div class="form-group">
                        <label for="exampleInputNomorHp">Nomor HP</label>
                        <input type="text" class="form-control @error('nomorhp') is-invalid @enderror" placeholder="Input Nomor HP" name="nomorhp" value="{{old('nomorhp')}}">
                        @error('nomorhp') <span class="text-danger">{{$message}}</span> @enderror
                    </div>
                    <div class="form-group">
                        <label for="exampleInputIdTelegram">ID Telegram</label>
                        <input type="text" class="form-control @error('idtelegram') is-invalid @enderror" placeholder="Input ID Telegram" name="idtelegram" value="{{old('idtelegram')}}">
                        @error('idtelegram') <span class="text-danger">{{$message}}</span> @enderror
                    </div>
                    <div class="form-group">
                        <label for="exampleInputAlamat">Alamat</label>
                        <input type="text" class="form-control @error('alamat') is-invalid @enderror" placeholder="Input Alamat" name="alamat" value="{{old('alamat')}}">
                        @error('alamat') <span class="text-danger">{{$message}}</span> @enderror
                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                    <a href="{{route('dosen.index')}}" class="btn btn-default">
                        Batal
                    </a>
                </div>
            </div>
        </div>
    </div>
@stop