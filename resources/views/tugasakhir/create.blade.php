@extends('adminlte::page')

@section('title', 'Tambah Data Tugas Akhir')

@section('content_header')
    <h1 class="m-0 text-darkj">Tambah Data Tugas Akhir</h1>
@stop
@section('content')
    <form action="{{route('tugasakhir.store')}}" method="post">
        @csrf
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">

                    <div class="form-group">
                        <label for="exampleInputNamaMahasiswa">Nama Mahasiswa</label>
                        <input type="text" class="form-control @error('namamhs') is-invalid @enderror" id="exampleInputNamaMahasiswa" placeholder="Input Nama Mahasiswa" name="namamhs" value="{{old('namamhs')}}">
                        @error('namamhs') <span class="text-danger">{{$message}}</span> @enderror
                    </div>
                    <div class="form-group">
                        <label for="exampleInputNimMahasiswa">NIM Mahasiswa</label>
                        <input type="text" class="form-control @error('nimmhs') is-invalid @enderror" id="exampleInputNimMahasiswa" placeholder="Input NIM Mahasiswa" name="nimmhs" value="{{old('nimmhs')}}">
                        @error('nimmhs') <span class="text-danger">{{$message}}</span> @enderror
                    </div>
                    <div class="form-group">
                        <label for="exampleInputDospemsatu">Dosen Pembimbing Satu</label>
                        <input type="text" class="form-control @error('dospemsatu') is-invalid @enderror" id="exampleInputDospemsatu" placeholder="Input Dosen Pembimbing Satu" name="dospemsatu" value="{{old('dospemsatu')}}">
                        @error('dospemsatu') <span class="text-danger">{{$message}}</span> @enderror
                    </div>
                    <div class="form-group">
                        <label for="exampleInputDospemdua">Dosen Pembimbing Dua</label>
                        <input type="text" class="form-control @error('dospemdua') is-invalid @enderror" id="exampleInputDospemdua" placeholder="Input Dosen Pembimbing Dua" name="dospemdua" value="{{old('dospemdua')}}">
                        @error('dospemdua') <span class="text-danger">{{$message}}</span> @enderror
                    </div>
                    <div class="form-group">
                        <label for="exampleInputTopikTa">Topik Tugas Akhir</label>
                        <input type="text" class="form-control @error('topikta') is-invalid @enderror" id="exampleInputTopikTa" placeholder="Input Topik Tugas Akhir" name="topikta" value="{{old('topikta')}}">
                        @error('topikta') <span class="text-danger">{{$message}}</span> @enderror
                    </div>
                    <div class="form-group">
                        <label for="exampleInputJudulTa">Judul Tugas Akhir</label>
                        <input type="text" class="form-control @error('judulta') is-invalid @enderror" id="exampleInputJudulTa" placeholder="Input Judul Tugas Akhir" name="judulta" value="{{old('judulta')}}">
                        @error('judulta') <span class="text-danger">{{$message}}</span> @enderror
                    </div>
                    <div class="form-group">
                        <label for="exampleInputTanggalMulai">Tanggal Mulai Tugas Akhir</label>
                        <input type="text" class="form-control @error('tanggalmulai') is-invalid @enderror" id="exampleInputTanggalMulai" placeholder="Input Judul Tugas Akhir" name="tanggalmulai" value="{{old('tanggalmulai')}}">
                        @error('tanggalmulai') <span class="text-danger">{{$message}}</span> @enderror
                    </div>
                    <div class="form-group">
                        <label for="exampleInputTanggalSelesai">Tanggal Selesai Tugas Akhir</label>
                        <input type="text" class="form-control @error('tanggsalselesai') is-invalid @enderror" id="exampleInputTanggalSelesai"  placeholder="Input Judul Tugas Akhir" name="tanggsalselesai" value="{{old('tanggsalselesai')}}">
                        @error('tanggsalselesai') <span class="text-danger">{{$message}}</span> @enderror
                    </div>
                       
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                    <a href="{{route('tugasakhir.index')}}" class="btn btn-default">
                        Batal
                    </a>
                </div>
            </div>
        </div>
    </div>
@stop