@extends('adminlte::page')

@section('title', 'Data Tugas Akhir')

@section('content_header')
<h1>Data Tugas Akhir</h1>
@stop
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                <a href="{{route('tugasakhir.create')}}" class="btn btn-primary mb-2">
                        Tambah Data Tugas Akhir
                    </a>
                    <table class="table table-hover table-bordered table-stripped" id="example2">
                        <thead>
                        <tr>
                            <th>No.</th>
                            <th>Nama</th>
                            <th>NIM</th>
                            <th>Dosen Pembimbing 1</th>
                            <th>Dosen Pembimbing 2</th>
                            <th>Topic</th>
                            <th>Judul</th>
                            <th>Tanggal Mulai</th>
                            <th>Tanggal Selesai</th>
                            <th>Opsi</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($tugasakhirs as $key => $tugasakhir)
                            <tr>
                                <td>{{$key+1}}</td>
                                <td>{{$tugasakhir->namamhs}}</td>
                                <td>{{$tugasakhir->nimmhs}}</td>
                                <td>{{$tugasakhir->dospemsatu}}</td>
                                <td>{{$tugasakhir->dospemdua}}</td>
                                <td>{{$tugasakhir->topikta}}</td>
                                <td>{{$tugasakhir->judulta}}</td>
                                <td>{{$tugasakhir->tanggalmulai}}</td>
                                <td>{{$tugasakhir->tanggsalselesai}}</td>
                                <td>
                                    <a href="{{route('tugasakhir.edit', $tugasakhir)}}" class="btn btn-primary btn-sm">
                                        Edit
                                    </a>
                                    <a href="{{route('tugasakhir.destroy', $tugasakhir)}}" onclick="notificationBeforeDelete(event, this)" class="btn btn-danger btn-sm">
                                        Delete
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop
@push('js')
    <form action="" id="delete-form" method="post">
        @method('delete')
        @csrf
    </form>
    <script>
        $('#example2').DataTable({
            "responsive": true,
        });
        function notificationBeforeDelete(event, el) {
            event.preventDefault();
            if (confirm('Apakah anda yakin akan menghapus data ? ')) {
                $("#delete-form").attr('action', $(el).attr('href'));
                $("#delete-form").submit();
            }
        }
    </script>
@endpush