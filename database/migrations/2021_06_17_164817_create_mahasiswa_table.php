<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMahasiswaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mahasiswas', function (Blueprint $table) {
            $table->id();
            $table->string('nimmhs')->nullable()->default(null);
            $table->string('namamhs')->nullable()->default(null);
            $table->string('angkatan')->nullable()->default(null);
            $table->string('email')->nullable()->default(null);
            $table->string('nomorhp')->nullable()->default(null);
            $table->string('idtelegram')->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mahasiswas');
    }
}
