<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDatatugasakhirTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tugasakhirs', function (Blueprint $table) {
            $table->id();
            $table->string('namamhs')->nullable()->default(null);
            $table->string('nimmhs')->nullable()->default(null);
            $table->string('dospemsatu')->nullable()->default(null);
            $table->string('dospemdua')->nullable()->default(null);
            $table->string('topikta')->nullable()->default(null);
            $table->string('judulta')->nullable()->default(null);
            $table->string('tanggalmulai')->nullable()->default(null);
            $table->string('tanggsalselesai')->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tugasakhirs');
    }
}
